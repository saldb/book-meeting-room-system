package com.example.meetingroomsbookingsystem;

import com.example.meetingroomsbookingsystem.models.Host;
import com.example.meetingroomsbookingsystem.models.Room;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class TestHost {


    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();


    @Test
    public void NoConstraintViolations() {
        Room room = new Room(4, 1, "Microsoft, USA.", Room.Size.medium);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room);
        Host host = new Host("Lama", "0505123123", "Software Engineer", "Google", rooms);
        Set<ConstraintViolation<Host>> violations = validator.validate(host);
        assertThat(violations.size()).isEqualTo(0);
    }

    @Test
    public void OneConstraintViolations() {
        Room room = new Room(4, 1, "Microsoft, USA.", Room.Size.medium);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room);
//        invalid mobile number
        Host host = new Host("de", "a050512312", "Software Engineer", "Google", rooms);
        Set<ConstraintViolation<Host>> violations = validator.validate(host);
        assertThat(violations.size()).isEqualTo(1);
    }
}
