package com.example.meetingroomsbookingsystem;

import com.example.meetingroomsbookingsystem.models.Room;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class TestRoom {

    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    public void NoConstraintViolations() {
        Room room = new Room(4, 1, "Microsoft, USA.", Room.Size.medium);
        Set<ConstraintViolation<com.example.meetingroomsbookingsystem.models.Room>> violations = validator.validate(room);
        assertThat(violations.size()).isEqualTo(0);
    }

    @Test
    public void OneConstraintViolations() {
        // the address length is 2. So, it violates the minimum 3.
        Room room = new com.example.meetingroomsbookingsystem.models.Room(4, 1, "US", com.example.meetingroomsbookingsystem.models.Room.Size.medium);
        Set<ConstraintViolation<com.example.meetingroomsbookingsystem.models.Room>> violations = validator.validate(room);
        assertThat(violations.size()).isEqualTo(1);
    }
}
