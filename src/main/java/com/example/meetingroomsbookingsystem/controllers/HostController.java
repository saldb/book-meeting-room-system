package com.example.meetingroomsbookingsystem.controllers;


import com.example.meetingroomsbookingsystem.models.Host;
import com.example.meetingroomsbookingsystem.services.impl.HostServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/hosts")
public class HostController {


    @Autowired
    HostServiceImpl hostService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Host addHost(@RequestBody Host host){
        return this.hostService.addHost(host);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Host> getAllHosts(){
        return this.hostService.getAllHosts();
    }

    @GetMapping(path = "{id}")
    public Host getHostById(@PathVariable(name = "id", required = true) Integer id){
        return this.hostService.getHostById(id);
    }
}
