package com.example.meetingroomsbookingsystem.controllers;


import com.example.meetingroomsbookingsystem.models.Room;
import com.example.meetingroomsbookingsystem.services.impl.RoomServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/rooms")
public class RoomController {

    @Autowired
    RoomServiceImpl roomService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Room addRoom(@RequestBody Room room) {
        return roomService.addRoom(room);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Room> getAllRooms() {
        return this.roomService.getAllRooms();
    }

    @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Room getRoomById(@PathVariable(name = "id", required = true) Integer id) {
        return this.roomService.getRoomById(id);
    }
//
//    @DeleteMapping
//    public void deleteRoomById(@RequestParam(name = "id", required = true) Integer id) {
//        this.roomService.deleteRoomById(id);
//    }
}
