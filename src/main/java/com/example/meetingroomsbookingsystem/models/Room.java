package com.example.meetingroomsbookingsystem.models;


import javax.persistence.*;
import java.util.Date;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Room {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    @NotNull
    @Digits(integer=2, fraction=0, message="Invalid Floor")
    private int floor;

    @NotNull
    @Digits(integer=3, fraction=0, message="Invalid Room Number")
    private int number;

    @javax.validation.constraints.Size(min=3, max = 200, message = "The minimum is 3 and maximum is 200")
    private String address;

    @NotNull
    private Size size;

    private Date createdAt;

    private Integer hostId;


    @PrePersist
    void createdAt() {
        this.createdAt = new Date(); }

    public static enum Size {
        small, medium, large, extraLarge
    }

    public Room() {
    }

    public Room(@NotNull @Digits(integer = 2, fraction = 0, message = "Invalid Floor") int floor, @NotNull @Digits(integer = 3, fraction = 0, message = "Invalid Room Number") int number, @javax.validation.constraints.Size(min = 10, max = 200, message = "The minimum is 10 and maximum is 200") @Pattern(regexp = "(^$|[A-z])") String address, @NotNull Size size) {
        this.floor = floor;
        this.number = number;
        this.address = address;
        this.size = size;
        createdAt();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getHostId() {
        return hostId;
    }

    public void setHostId(Integer hostId) {
        this.hostId = hostId;
    }
}
