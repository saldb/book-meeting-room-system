package com.example.meetingroomsbookingsystem.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Host {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @NotNull
    @javax.validation.constraints.Size(min=2, max = 100, message = "The minimum is 2 and maximum is 100")
    private String name;

    @NotNull
    @javax.validation.constraints.Size(min=2, max = 100, message = "The minimum is 2 and maximum is 100")
    @Pattern(regexp="([0-9]{10})")
    private String mobileNumber;

    @NotNull
    @javax.validation.constraints.Size(min=1, max = 100, message = "The minimum is 1 and maximum is 100")
    private String JobTitle;

    @NotNull
    @javax.validation.constraints.Size(min=3, max = 100, message = "The minimum is 3 and maximum is 100")
    private String companyName;


    @OneToMany(mappedBy = "hostId")
    private List<Room> rooms;


    public Host() {
    }

    public Host(@NotNull @Size(min = 2, max = 100, message = "The minimum is 2 and maximum is 100") String name, @NotNull @Size(min = 2, max = 100, message = "The minimum is 2 and maximum is 100") @Pattern(regexp = "([0-9]{10})") String mobileNumber, @NotNull @Size(min = 1, max = 100, message = "The minimum is 1 and maximum is 100") String jobTitle, @NotNull @Size(min = 3, max = 100, message = "The minimum is 3 and maximum is 100") String companyName, @NotNull List<Room> rooms) {
        this.name = name;
        this.mobileNumber = mobileNumber;
        JobTitle = jobTitle;
        this.companyName = companyName;
        this.rooms = rooms;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getJobTitle() {
        return JobTitle;
    }

    public void setJobTitle(String jobTitle) {
        JobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public Integer getId() {
        return id;
    }
}
