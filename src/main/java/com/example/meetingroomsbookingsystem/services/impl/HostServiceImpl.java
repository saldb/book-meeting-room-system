package com.example.meetingroomsbookingsystem.services.impl;


import com.example.meetingroomsbookingsystem.models.Host;
import com.example.meetingroomsbookingsystem.repositories.HostRepository;
import com.example.meetingroomsbookingsystem.services.HostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HostServiceImpl implements HostService {

    @Autowired
    HostRepository hostRepository;

    @Autowired
    public HostServiceImpl(HostRepository hostRepository) {
        this.hostRepository = hostRepository;
    }


    public Host addHost(Host host) {
        this.hostRepository.save(host);
        if (this.hostRepository.existsById(host.getId()))
            return host;
        else
            return null;
    }

    public List<Host> getAllHosts(){
        return this.hostRepository.findAll();
    }

    public Host getHostById(Integer id){
        return this.hostRepository.getOne(id);
    }
}
