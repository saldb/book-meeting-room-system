package com.example.meetingroomsbookingsystem.services.impl;

import com.example.meetingroomsbookingsystem.models.Room;
import com.example.meetingroomsbookingsystem.repositories.RoomRepository;
import com.example.meetingroomsbookingsystem.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    public RoomServiceImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public Optional<Room> findByHostId(Integer id) {
        return this.roomRepository.findByHostId(id);
    }

    public Room addRoom(Room room) {
        this.roomRepository.save(room);
        if (this.roomRepository.existsById(room.getId()))
            return room;
        else
            return null;
    }

    public List<Room> getAllRooms() {
        return this.roomRepository.findAll();
    }

    public Room getRoomById(Integer id) {
        return this.roomRepository.getOne(id);
    }

    public void deleteRoomById(Integer id) {
        this.roomRepository.delete(this.getRoomById(id));
    }
}
