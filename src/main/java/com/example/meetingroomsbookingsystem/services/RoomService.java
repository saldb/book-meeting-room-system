package com.example.meetingroomsbookingsystem.services;

import com.example.meetingroomsbookingsystem.models.Room;
import org.springframework.stereotype.Service;

import java.util.Optional;

public interface RoomService {
    Optional<Room> findByHostId(Integer id);
}
