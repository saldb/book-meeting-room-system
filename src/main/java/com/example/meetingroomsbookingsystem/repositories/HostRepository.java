package com.example.meetingroomsbookingsystem.repositories;

import com.example.meetingroomsbookingsystem.models.Host;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HostRepository extends JpaRepository<Host, Integer> {

}
