package com.example.meetingroomsbookingsystem.repositories;

import com.example.meetingroomsbookingsystem.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    Optional<Room> findByHostId(@Param("hostId") Integer hostId);
}
