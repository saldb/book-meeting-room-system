package com.example.meetingroomsbookingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeetingRoomsBookingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeetingRoomsBookingSystemApplication.class, args);
	}

}
